(function($) {
    Drupal.behaviors.sample = {
        attach: function(context) {
            jQuery('form[id^="otp-form"] .resend-button').click(function() {
                jQuery(this).hide();
            });

            var otp_id = jQuery.cookie('otp_id');
            if (otp_id != null && !jQuery('.otp-link').hasClass('triggered')) {
                jQuery.cookie('otp_id', null, {path: Drupal.settings.basePath});
                var otp_link = jQuery('.otp-link');
                otp_link.replaceWith('<a class="otp-link hide-field ctools-use-modal ctools-modal-otp-style" href="' + Drupal.settings.basePath + 'generate-otp/nojs/' + otp_id + '">Validate OTP</a>');
                //otp_link.removeClass();
                var url = Drupal.settings.basePath + 'generate-otp/nojs/' + otp_id;
                otp_link.unbind();
                otp_link.click(Drupal.CTools.Modal.clickAjaxLink);
                var element_settings = {};
                element_settings.url = url;
                element_settings.event = 'click';
                element_settings.progress = {type: 'throbber'};
                var base = url;
                Drupal.ajax[base] = new Drupal.ajax(base, '.otp-link', element_settings);
                //otp_link.addClass('ctools-modal-otp-style');
                //otp_link.addClass('ctools-use-modal-processed');
                jQuery('.otp-link').trigger('click');
                jQuery('.otp-link').addClass('triggered');
            }
            var remtime = jQuery('[id^=otp-form] .js-timer', context).attr('data-time');
            if (remtime) {
                remtime = parseInt(remtime) * 1000;
                $('[id^=otp-form] .js-timer').countdown({
                    date: new Date(Date.now() + remtime),
                    render: function(data) {
                        var seconds = (data.days * 86400) + (data.hours * 3600) + (data.min * 60) + data.sec;
                        $(this.el).text(this.leadingZeros(seconds));
                    },
                    onEnd: function() {
                        $('.otp-expire-message').remove();
                        $(this.el).after('<span class="otp-expire-message">OTP is expired. Kindly click on Resend OTP button.</span>');
                        $(this.el).addClass('ended');
                    }
                });
            }
        }
    };
})(jQuery);
