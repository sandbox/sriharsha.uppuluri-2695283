Its a sandbox project.

Using this module:

function hook_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'YOUR_FORMID') {
    $form['#submit'][] = 'sample_otp_generate';
  }
}

function sample_otp_generate($form, $form_state) {
  otp_generate_otp('MOBILE_NUMBER', 'FORMID');
}

In your form or page need to print this as a link and hide it:

print l(t('Validate OTP'), 'generate-otp/nojs', array('attributes' => array('class' => 'otp-link hide-field ctools-use-modal ctools-modal-otp-style')));